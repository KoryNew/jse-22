package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTasksByProjectId(final String id, final String userId) throws EmptyIdException;

    Task bindTaskToProject(final String projectId, final String taskId, final String userId) throws AbstractException;

    Task unbindTaskFromProject(final String projectId, final String taskId, final String userId) throws AbstractException;

}
