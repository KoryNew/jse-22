package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.entity.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    Optional<User> findByLogin(final String login);

    Optional<User> findByEmail(final String email);

    Optional<User> removeByLogin(final String login);

}
