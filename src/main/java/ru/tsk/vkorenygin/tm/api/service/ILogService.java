package ru.tsk.vkorenygin.tm.api.service;

public interface ILogService {

    void info(final String message);

    void debug(final String message);

    void command(final String message);

    void error(final Exception e);

}
