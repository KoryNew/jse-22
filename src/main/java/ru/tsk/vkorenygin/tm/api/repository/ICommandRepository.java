package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    Collection<String> getCommandNames();

    Collection<String> getCommandArgs();

    AbstractCommand getCommandByName(final String name);

    AbstractCommand getCommandByArg(final String name);

    void add(final AbstractCommand command);


}
