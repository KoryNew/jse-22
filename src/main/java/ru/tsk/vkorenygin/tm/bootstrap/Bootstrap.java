package ru.tsk.vkorenygin.tm.bootstrap;

import ru.tsk.vkorenygin.tm.api.repository.ICommandRepository;
import ru.tsk.vkorenygin.tm.api.repository.IProjectRepository;
import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.api.repository.IUserRepository;
import ru.tsk.vkorenygin.tm.api.service.*;
import ru.tsk.vkorenygin.tm.command.AbstractCommand;
import ru.tsk.vkorenygin.tm.command.project.*;
import ru.tsk.vkorenygin.tm.command.system.*;
import ru.tsk.vkorenygin.tm.command.task.*;
import ru.tsk.vkorenygin.tm.command.user.*;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.system.UnknownArgumentException;
import ru.tsk.vkorenygin.tm.exception.system.UnknownCommandException;
import ru.tsk.vkorenygin.tm.repository.CommandRepository;
import ru.tsk.vkorenygin.tm.repository.ProjectRepository;
import ru.tsk.vkorenygin.tm.repository.TaskRepository;
import ru.tsk.vkorenygin.tm.repository.UserRepository;
import ru.tsk.vkorenygin.tm.service.*;
import ru.tsk.vkorenygin.tm.util.DataUtil;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

public class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IUserRepository userRepository = new UserRepository();
    private final IUserService userService = new UserService(userRepository);
    private final IAuthService authService = new AuthService(userService);

    private final ILogService logService = new LogService();

    private String adminId = null;
    private String userId = null;

    {
        registry(new CommandsCommand());
        registry(new ArgumentsCommand());
        registry(new AboutCommand());
        registry(new HelpCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new ExitCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectListCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectStatusChangeByIdCommand());
        registry(new ProjectStatusChangeByIndexCommand());
        registry(new ProjectStatusChangeByNameCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskListCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskStatusChangeByIdCommand());
        registry(new TaskStatusChangeByIndexCommand());
        registry(new TaskStatusChangeByNameCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserShowProfileCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserLockByLoginCommand());
        registry(new UserUnlockByLoginCommand());
        registry(new UserRemoveByLoginCommand());
    }

    private void registry(final AbstractCommand command) {
        if (command == null)
            return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private boolean parseArgs(final String[] args) throws AbstractException {
        if (DataUtil.isEmpty(args))
            return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseArg(final String arg) throws AbstractException {
        if (DataUtil.isEmpty(arg))
            return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null)
            throw new UnknownArgumentException();
        command.execute();
    }

    private void parseCommand(final String cmd) throws AbstractException {
        if (DataUtil.isEmpty(cmd))
            throw new UnknownCommandException();
        final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null)
            throw new UnknownCommandException();
        final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    public void run(final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        initUsers();
        initData();
        if (parseArgs(args))
            System.exit(0);
        process();
    }

    private void process() {
        logService.debug("Test environment.");
        String command = "";
        while (true) {
            try {
                logService.command(command);
                command = TerminalUtil.nextLine();
                parseCommand(command);
                logService.info("Commands '" + command + "' is executed.");
                System.out.println("[OK]");
            } catch (Exception e) {
                logService.error(e);
                System.out.println("[ERROR]");
            }
        }
    }

    private void initData() {
        try {
            projectService.create("Project C", "Desc 1", adminId);
            projectService.create("Project A", "Desc 2", userId);
            projectService.startByIndex(0, adminId);
            projectService.finishByIndex(0, userId);
            taskService.create("Task C", "Desc 1", adminId);
            taskService.create("Task A", "Desc 2", userId);
            taskService.finishByIndex(0, adminId);
            taskService.startByIndex(0, userId);
        } catch (RuntimeException e) {
            logService.error(e);
        }
    }

    private void initUsers() throws AbstractException {
        userId = userService.create("test", "test", "test@test.ru").getId();
        adminId = userService.create("admin", "admin", Role.ADMIN).getId();
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
