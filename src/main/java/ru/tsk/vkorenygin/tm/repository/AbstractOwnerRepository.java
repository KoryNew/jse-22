package ru.tsk.vkorenygin.tm.repository;

import ru.tsk.vkorenygin.tm.api.repository.IOwnerRepository;
import ru.tsk.vkorenygin.tm.entity.AbstractOwnerEntity;
import ru.tsk.vkorenygin.tm.exception.AbstractException;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    protected Predicate<E> hasUser(final String userId) {
        return e -> userId.equals(e.getUserId());
    }

    public boolean existsById(final String id, final String userId) {
        return entities.stream()
                .filter(hasUser(userId))
                .anyMatch(hasId(id));
    }

    public boolean existsByIndex(final Integer index, final String userId) {
        if (index < 0) return false;
        return index < entities.size();
    }

    @Override
    public Optional<E> remove(final E entity, final String userId) {
        if (!userId.equals(entity.getUserId())) return Optional.empty();
        entities.remove(entity);
        return Optional.of(entity);
    }

    @Override
    public void clear(String userId) {
        entities.stream()
                .filter(hasUser(userId))
                .forEach(e -> entities.remove(e));
    }

    @Override
    public List<E> findAll(final String userId) {
        return entities.stream()
                .filter(hasUser(userId))
                .collect(Collectors.toList());
    }

    public List<E> findAll(final Comparator<E> comparator, String userId) {
        return entities.stream()
                .filter(hasUser(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<E> findById(final String id, final String userId) throws AbstractException {
        return entities.stream()
                .filter(hasUser(userId))
                .filter(hasId(id))
                .findFirst();
    }

    @Override
    public Optional<E> findByIndex(final int index, final String userId) throws AbstractException {
        return entities.stream()
                .filter(hasUser(userId))
                .skip(index)
                .findFirst();
    }

    @Override
    public Optional<E> removeById(final String id, final String userId) throws AbstractException {
        Optional<E> entity = findById(id, userId);
        entity.ifPresent(e -> entities.remove(e));
        return entity;
    }

    @Override
    public Optional<E> removeByIndex(final int index, final String userId) throws AbstractException {
        Optional<E> entity = findByIndex(index, userId);
        entity.ifPresent(e -> entities.remove(e));
        return entity;
    }

}
