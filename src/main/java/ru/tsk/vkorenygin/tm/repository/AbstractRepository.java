package ru.tsk.vkorenygin.tm.repository;

import ru.tsk.vkorenygin.tm.api.repository.IRepository;
import ru.tsk.vkorenygin.tm.entity.AbstractEntity;

import java.util.*;
import java.util.function.Predicate;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected List<E> entities = new ArrayList<>();

    protected Predicate<E> hasId(String id) {
        return e -> id.equals(e.getId());
    }

    @Override
    public E add(final E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public void addAll(final Collection<E> collection) {
        collection.forEach(this::add);
    }

    @Override
    public boolean existsById(final String id) {
        return entities.stream()
                .anyMatch(hasId(id));
    }

    @Override
    public boolean existsByIndex(final Integer index) {
        return index < entities.size();
    }

    @Override
    public int getSize() {
        return entities.size();
    }

    @Override
    public List<E> findAll() {
        return entities;
    }

    public List<E> findAll(final Comparator<E> comparator) {
        final List<E> entitiesSorted = new ArrayList<>(entities);
        entitiesSorted.sort(comparator);
        return entitiesSorted;
    }

    @Override
    public Optional<E> findById(final String id) {
        return entities.stream()
                .filter(hasId(id))
                .findFirst();
    }

    @Override
    public Optional<E> findByIndex(final Integer index) {
        return entities.stream()
                .skip(index)
                .findFirst();
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public Optional<E> removeById(final String id) {
        final Optional<E> entity = findById(id);
        entity.ifPresent(this::remove);
        return entity;
    }

    @Override
    public Optional<E> removeByIndex(final Integer index) {
        final Optional<E> entity = findByIndex(index);
        entity.ifPresent(this::remove);
        return entity;
    }

    @Override
    public Optional<E> remove(final E entity) {
        entities.remove(entity);
        return Optional.ofNullable(entity);
    }

}
