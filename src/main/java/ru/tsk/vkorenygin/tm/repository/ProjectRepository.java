package ru.tsk.vkorenygin.tm.repository;

import ru.tsk.vkorenygin.tm.api.repository.IProjectRepository;
import ru.tsk.vkorenygin.tm.entity.Project;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;

import java.util.Date;
import java.util.Optional;
import java.util.function.Predicate;

public class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    protected Predicate<Project> hasName(final String name) {
        return e -> name.equals(e.getUserId());
    }

    @Override
    public Optional<Project> findByName(final String name, final String userId) {
        return entities.stream()
                .filter(hasUser(userId))
                .filter(hasName(name))
                .findFirst();
    }

    @Override
    public Project changeStatusById(final String id, final Status status, final String userId) {
        final Optional<Project> project = findById(id, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(status));
        if (status == Status.IN_PROGRESS) project.ifPresent(e -> e.setStartDate(new Date()));
        return project.get();
    }

    @Override
    public Project changeStatusByName(final String name, final Status status, final String userId) {
        final Optional<Project> project = findByName(name, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(status));
        if (status == Status.IN_PROGRESS) project.ifPresent(e -> e.setStartDate(new Date()));
        return project.get();
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status, final String userId) {
        final Optional<Project> project = findByIndex(index, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(status));
        if (status == Status.IN_PROGRESS) project.ifPresent(e -> e.setStartDate(new Date()));
        return project.get();

    }

    @Override
    public Project startById(final String id, final String userId) {
        final Optional<Project> project = findById(id, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> {
            e.setStatus(Status.IN_PROGRESS);
            e.setStartDate(new Date());
        });
        return project.get();
    }

    @Override
    public Project startByIndex(final Integer index, final String userId) {
        final Optional<Project> project = findByIndex(index, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> {
            e.setStatus(Status.IN_PROGRESS);
            e.setStartDate(new Date());
        });
        return project.get();
    }

    @Override
    public Project startByName(final String name, final String userId) {
        final Optional<Project> project = findByName(name, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> {
            e.setStatus(Status.IN_PROGRESS);
            e.setStartDate(new Date());
        });
        return project.get();
    }

    @Override
    public Project finishById(final String id, final String userId) {
        final Optional<Project> project = findById(id, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return project.get();
    }

    @Override
    public Project finishByIndex(final Integer index, final String userId) {
        final Optional<Project> project = findByIndex(index, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return project.get();
    }

    @Override
    public Project finishByName(final String name, final String userId) {
        final Optional<Project> project = findByName(name, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return project.get();
    }

    @Override
    public Optional<Project> removeByName(final String name, final String userId) {
        Optional<Project> project = findByName(userId, name);
        project.ifPresent(e -> entities.remove(e));
        return project;
    }

}
