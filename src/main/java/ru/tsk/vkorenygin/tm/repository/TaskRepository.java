package ru.tsk.vkorenygin.tm.repository;

import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.vkorenygin.tm.exception.entity.TaskNotFoundException;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    protected Predicate<Task> hasName(final String name) {
        return e -> name.equals(e.getUserId());
    }

    protected Predicate<Task> hasProjectId(final String projectId) {
        return e -> projectId.equals(e.getProjectId());
    }

    @Override
    public Optional<Task> findByName(final String name, final String userId) {
        return entities.stream()
                .filter(hasUser(userId))
                .filter(hasName(name))
                .findFirst();
    }

    @Override
    public Task changeStatusById(final String id, final Status status, final String userId) {
        final Optional<Task> task = findById(id, userId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setStatus(status));
        if (status == Status.IN_PROGRESS) task.ifPresent(e -> e.setStartDate(new Date()));
        return task.get();
    }

    @Override
    public Task changeStatusByName(final String name, final Status status, final String userId) {
        final Optional<Task> task = findByName(name, userId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setStatus(status));
        if (status == Status.IN_PROGRESS) task.ifPresent(e -> e.setStartDate(new Date()));
        return task.get();
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status, final String userId) {
        final Optional<Task> task = findByIndex(index, userId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setStatus(status));
        if (status == Status.IN_PROGRESS) task.ifPresent(e -> e.setStartDate(new Date()));
        return task.get();
    }

    @Override
    public Task startById(final String id, final String userId) {
        final Optional<Task> task = findById(id, userId);
        if (!task.isPresent()) throw new ProjectNotFoundException();
        task.ifPresent(e -> {
            e.setStatus(Status.IN_PROGRESS);
            e.setStartDate(new Date());
        });
        return task.get();
    }

    @Override
    public Task startByIndex(final Integer index, final String userId) {
        final Optional<Task> task = findByIndex(index, userId);
        if (!task.isPresent()) throw new ProjectNotFoundException();
        task.ifPresent(e -> {
            e.setStatus(Status.IN_PROGRESS);
            e.setStartDate(new Date());
        });
        return task.get();
    }

    @Override
    public Task startByName(final String name, final String userId) {
        final Optional<Task> task = findByName(name, userId);
        if (!task.isPresent()) throw new ProjectNotFoundException();
        task.ifPresent(e -> {
            e.setStatus(Status.IN_PROGRESS);
            e.setStartDate(new Date());
        });
        return task.get();
    }

    @Override
    public Task finishById(final String id, final String userId) {
        final Optional<Task> task = findById(id, userId);
        if (!task.isPresent()) throw new ProjectNotFoundException();
        task.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return task.get();
    }

    @Override
    public Task finishByIndex(final Integer index, final String userId) {
        final Optional<Task> task = findByIndex(index, userId);
        if (!task.isPresent()) throw new ProjectNotFoundException();
        task.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return task.get();
    }

    @Override
    public Task finishByName(final String name, final String userId) {
        final Optional<Task> task = findByName(name, userId);
        if (!task.isPresent()) throw new ProjectNotFoundException();
        task.ifPresent(e -> e.setStatus(Status.COMPLETED));
        return task.get();
    }

    @Override
    public Task bindTaskToProjectById(final String projectId, final String taskId, final String userId) {
        Optional<Task> task = findById(taskId, userId);
        task.ifPresent(e -> e.setProjectId(projectId));
        return task.get();
    }

    @Override
    public Task unbindTaskById(final String id, final String userId) {
        Optional<Task> task = findById(id, userId);
        task.ifPresent(e -> e.setProjectId(null));
        return task.get();
    }

    @Override
    public List<Task> findAllByProjectId(final String id, final String userId) {
        return entities.stream()
                .filter(hasUser(userId))
                .filter(hasProjectId(id))
                .collect(Collectors.toList());
    }

    @Override
    public void removeAllTaskByProjectId(final String id, final String userId) {
        entities.stream()
                .filter(hasUser(id))
                .forEach(this::remove);
    }

    @Override
    public Optional<Task> removeByName(final String name, String userId) {
        Optional<Task> task = findByName(userId, name);
        task.ifPresent(e -> entities.remove(e));
        return task;
    }

}
