package ru.tsk.vkorenygin.tm.enumerated;

public enum Status {

    IN_PROGRESS("In progress", 1),
    NOT_STARTED("Not started", 2),
    COMPLETED("Completed", 3);

    private final int priority;
    private final String displayName;

    Status(String displayName, int priority) {
        this.displayName = displayName;
        this.priority = priority;
    }

    public String getDisplayName() {
        return displayName;
    }

    public int getPriority() {
        return priority;
    }

}
