package ru.tsk.vkorenygin.tm.command.project;

import ru.tsk.vkorenygin.tm.command.AbstractProjectCommand;
import ru.tsk.vkorenygin.tm.enumerated.Role;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "remove all projects";
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        final String currentUserId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectService().clear(currentUserId);
    }

}
