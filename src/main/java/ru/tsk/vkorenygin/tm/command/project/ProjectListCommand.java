package ru.tsk.vkorenygin.tm.command.project;

import ru.tsk.vkorenygin.tm.command.AbstractProjectCommand;
import ru.tsk.vkorenygin.tm.entity.Project;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.enumerated.Sort;
import ru.tsk.vkorenygin.tm.util.EnumerationUtil;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-list";
    }

    @Override
    public String description() {
        return "show project list";
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

    @Override
    public void execute() {
        final String currentUserId = serviceLocator.getAuthService().getUserId();
        System.out.println("[LIST PROJECTS]");
        System.out.println("ENTER SORT: ");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        Sort sortType = EnumerationUtil.parseSort(sort);

        List<Project> projects;
        projects = sortType == null ?
                serviceLocator.getProjectService().findAll(currentUserId) :
                serviceLocator.getProjectService().findAll(sortType.getComparator(), currentUserId);

        int index = 1;
        for (final Project project : projects) {
            final String projectStatus = project.getStatus().getDisplayName();
            System.out.println(index + ". " + project + " (" + projectStatus + ")");
            index++;
        }
    }

}
