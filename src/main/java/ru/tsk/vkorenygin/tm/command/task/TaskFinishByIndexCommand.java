package ru.tsk.vkorenygin.tm.command.task;

import ru.tsk.vkorenygin.tm.command.AbstractTaskCommand;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.entity.TaskNotFoundException;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

public class TaskFinishByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-finish-by-index";
    }

    @Override
    public String description() {
        return "finish task by index";
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().finishByIndex(index, currentUserId);
        if (task == null) throw new TaskNotFoundException();
    }

}
