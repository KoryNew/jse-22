package ru.tsk.vkorenygin.tm.command;

import ru.tsk.vkorenygin.tm.entity.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    public void show(User user) {
        System.out.println("Login: " + user.getLogin());
        System.out.println("E-mail: " + user.getEmail());
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("Middle name: " + user.getMiddleName());
    }

}
