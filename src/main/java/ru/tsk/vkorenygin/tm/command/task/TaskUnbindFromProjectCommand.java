package ru.tsk.vkorenygin.tm.command.task;

import ru.tsk.vkorenygin.tm.command.AbstractTaskCommand;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-unbind-by-project-id";
    }

    @Override
    public String description() {
        return "unbind task from project by id";
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.USER};
    }

    @Override
    public void execute() throws AbstractException {
        final String currentUserId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UNBIND TASK FROM PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findById(projectId, currentUserId);
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        serviceLocator.getTaskService().findById(taskId, currentUserId);
        serviceLocator.getProjectTaskService().unbindTaskFromProject(projectId, taskId, currentUserId);
    }

}
