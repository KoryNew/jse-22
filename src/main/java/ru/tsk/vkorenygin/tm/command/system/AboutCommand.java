package ru.tsk.vkorenygin.tm.command.system;

import ru.tsk.vkorenygin.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String description() {
        return "display developer info";
    }

    @Override
    public void execute() {
        System.out.println("- ABOUT -");
        System.out.println("Developed by: Vladimir Korenyugin");
        System.out.println("E-mail: vkorenygin@tsconsulting.com");
    }

}
