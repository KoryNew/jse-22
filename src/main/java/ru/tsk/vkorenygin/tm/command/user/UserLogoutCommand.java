package ru.tsk.vkorenygin.tm.command.user;

import ru.tsk.vkorenygin.tm.command.AbstractUserCommand;

public class UserLogoutCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String description() {
        return "logout from system";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

}
