package ru.tsk.vkorenygin.tm.command.user;

import ru.tsk.vkorenygin.tm.command.AbstractUserCommand;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

public class UserUnlockByLoginCommand extends AbstractUserCommand {
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    public String description() {
        return "unlock user by login";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockByLogin(login);
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }

}
