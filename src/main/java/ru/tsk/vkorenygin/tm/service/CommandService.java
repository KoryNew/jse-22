package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.ICommandRepository;
import ru.tsk.vkorenygin.tm.api.service.ICommandService;
import ru.tsk.vkorenygin.tm.command.AbstractCommand;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Collection;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public Collection<String> getCommandNames() {
        return commandRepository.getCommandNames();
    }

    @Override
    public Collection<String> getCommandArgs() {
        return commandRepository.getCommandArgs();
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        if (DataUtil.isEmpty(name)) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public AbstractCommand getCommandByArg(final String name) {
        if (DataUtil.isEmpty(name)) return null;
        return commandRepository.getCommandByArg(name);
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

}
