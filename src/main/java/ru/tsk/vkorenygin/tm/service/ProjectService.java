package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.IProjectRepository;
import ru.tsk.vkorenygin.tm.api.service.IProjectService;
import ru.tsk.vkorenygin.tm.entity.Project;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyDescriptionException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyNameException;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Optional;

public class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String name, final String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        projectRepository.add(project);
    }

    @Override
    public void create(final String name, final String description, final String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        if (DataUtil.isEmpty(description))
            throw new EmptyDescriptionException();
        Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectRepository.add(project);
    }

    @Override
    public Optional<Project> findByIndex(final int index, final String userId) throws AbstractException {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index - 1 > projectRepository.getSize())
            throw new ProjectNotFoundException();
        return projectRepository.findByIndex(index, userId);
    }

    @Override
    public Optional<Project> findByName(final String name, final String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return projectRepository.findByName(name, userId);
    }

    @Override
    public Project changeStatusById(final String id, final Status status, final String userId) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        if (status == null)
            return null;
        return projectRepository.changeStatusById(id, status, userId);
    }

    @Override
    public Project changeStatusByName(final String name, final Status status, final String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        if (status == null)
            return null;
        return projectRepository.changeStatusByName(name, status, userId);
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status, final String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index - 1 > projectRepository.getSize())
            throw new ProjectNotFoundException();
        if (status == null)
            return null;
        return projectRepository.changeStatusByIndex(index, status, userId);
    }

    @Override
    public Project startById(final String id, final String userId) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return projectRepository.startById(id, userId);
    }

    @Override
    public Project startByIndex(final Integer index, final String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index - 1 > projectRepository.getSize())
            throw new ProjectNotFoundException();
        return projectRepository.startByIndex(index, userId);
    }

    @Override
    public Project startByName(final String name, final String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return projectRepository.startByName(name, userId);
    }

    @Override
    public Project finishById(final String id, final String userId) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return projectRepository.finishById(id, userId);
    }

    @Override
    public Project finishByIndex(final Integer index, final String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index - 1 > projectRepository.getSize())
            throw new ProjectNotFoundException();
        return projectRepository.finishByIndex(index, userId);
    }

    @Override
    public Project finishByName(final String name, final String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return projectRepository.finishByName(name, userId);
    }

    @Override
    public Project updateByIndex(Integer index,
                                 String name,
                                 String description,
                                 String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index - 1 > projectRepository.getSize())
            throw new ProjectNotFoundException();
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        Optional<Project> project = projectRepository.findByIndex(index, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setName(name));
        project.ifPresent(e -> e.setDescription(description));
        return project.get();
    }

    @Override
    public Project updateById(String id,
                              String name,
                              String description,
                              String userId) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        Optional<Project> project = projectRepository.findById(id, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setName(name));
        project.ifPresent(e -> e.setName(description));
        return project.get();
    }

    @Override
    public Optional<Project> removeByIndex(final int index, final String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        Optional<Project> project = projectRepository.findByIndex(index, userId);
        if (!project.isPresent())
            throw new ProjectNotFoundException();
        return projectRepository.removeByIndex(index, userId);
    }

    @Override
    public Optional<Project> removeByName(final String name, final String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        Optional<Project> project = projectRepository.findByName(name, userId);
        if (!project.isPresent())
            throw new ProjectNotFoundException();
        return projectRepository.removeByName(name, userId);
    }

}
