package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.api.service.ITaskService;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyDescriptionException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyNameException;
import ru.tsk.vkorenygin.tm.exception.entity.TaskNotFoundException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Optional;

public class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name, final String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        taskRepository.add(task);
    }

    @Override
    public void create(final String name, final String description, final String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        if (DataUtil.isEmpty(description))
            throw new EmptyDescriptionException();
        Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskRepository.add(task);
    }

    @Override
    public Optional<Task> findByIndex(final int index, final String userId) throws AbstractException {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index - 1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        return taskRepository.findByIndex(index, userId);
    }

    @Override
    public Optional<Task> findByName(final String name, final String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return taskRepository.findByName(name, userId);
    }

    @Override
    public Task changeStatusById(final String id, final Status status, final String userId) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        if (status == null)
            return null;
        return taskRepository.changeStatusById(id, status, userId);
    }

    @Override
    public Task changeStatusByName(final String name, final Status status, final String userId) {        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        if (status == null)
            return null;
        return taskRepository.changeStatusByName(name, status, userId);
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status, final String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index - 1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        if (status == null)
            return null;
        return taskRepository.changeStatusByIndex(index, status, userId);
    }

    @Override
    public Task startById(final String id, final String userId) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return taskRepository.startById(id, userId);
    }

    @Override
    public Task startByIndex(final Integer index, final String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index - 1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        return taskRepository.startByIndex(index, userId);
    }

    @Override
    public Task startByName(final String name, final String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return taskRepository.startByName(name, userId);
    }

    @Override
    public Task finishById(final String id, final String userId) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return taskRepository.finishById(id, userId);
    }

    @Override
    public Task finishByIndex(final Integer index, final String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index - 1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        return taskRepository.finishByIndex(index, userId);
    }

    @Override
    public Task finishByName(final String name, final String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return taskRepository.finishByName(name, userId);
    }

    @Override
    public Task updateByIndex(Integer index,
                              String name,
                              String description,
                              String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index - 1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        final Optional<Task> task = findByIndex(index, userId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setName(name));
        task.ifPresent(e -> e.setName(description));
        return task.get();
    }

    @Override
    public Task updateById(String id,
                           String name,
                           String description,
                           String userId) {
        if (DataUtil.isEmpty(id))
            return null;
        if (DataUtil.isEmpty(name))
            return null;
        Optional<Task> task = findById(id, userId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setName(name));
        task.ifPresent(e -> e.setName(description));
        return task.get();
    }

    @Override
    public Optional<Task> removeByIndex(final int index, final String userId) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index - 1 > taskRepository.getSize())
            throw new TaskNotFoundException();
        return taskRepository.removeByIndex(index, userId);
    }

    @Override
    public Optional<Task> removeByName(final String name, final String userId) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return taskRepository.removeByName(name, userId);
    }

}
